postfix
==========

Ansible playbook that installs and configfiures postfix on Debian

WARNING: You need to specify a FQDN in the host file as postfix installation fails with `fatal: unable to use my own hostname` in postconf.

```
Postfix requires that the myhostname setting satisfies Internet hostname syntax rules. 
See RFC 952, RFC 1123, RFC 1035, RFC 2373.
```

Role Variables
--------------

The desired behavior can be refined via variables.

Option | Description
--- | ---
`postfix_config_file` | Path to postfix config (default `/etc/postfix/main.cf`)
`postfix_service_state` | State the ufw should be in (default `started`)
`postfix_service_enabled` | Is the service enabled at boot (default `true`)
`postfix_inet_interfaces` | Values for inet_interfaces in the main.cf (default `localhost`)
`postfix_inet_protocols` | Values for inet_protocols in the main.cf  (default `all`)


Example Playbook
----------------

```yaml
# file: test.yml
- hosts: local
        
  roles:
    - postfix
```